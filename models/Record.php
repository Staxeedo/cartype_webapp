<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "record".
 *
 * @property int $recordID
 * @property string $spz
 * @property string $carType
 * @property string $tstamp
 */
class Record extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'record';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['spz', 'carType'], 'required'],
            [['carType'], 'string'],
            [['cam'], 'integer'],
            [['tstamp'], 'safe'],
            [['spz'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'recordID' => 'RecordID',
            'spz' => 'SPZ',
            'carType' => 'Car Type',
            'tstamp' => 'TimeStamp',
            'cam'=>'Cam'
        ];
    }
}
