<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Změna hesla';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-changepass">
    <h1><?=Html::encode($this->title)?></h1>

    <?php $form = ActiveForm::begin();?>


    <?=$form->field($model, 'password')->passwordInput()->label('Heslo')?>
    <?=$form->field($model, 'repeat')->passwordInput()->label('Zopakujte Heslo')?>
    <div class="form-group">
        <?=Html::submitButton('Změnit', ['class' => 'btn btn-primary', 'name' => 'signup-button'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
