<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RecordSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Records';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="record-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'foto',
                'label' => '',
                'format'=>'raw',
                'value' => function($model){
                    $recordID = $model->recordID;
                    $path="../../uploads/".$model->carType."/".$recordID.".jpg";
                   
                        return '<img src="'.$path.'" alt="'.$model->recordID.'" height=100px>';
                    
                }
            ],
            'recordID',
            'spz',
            'carType',
            'tstamp',
            'cam',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
