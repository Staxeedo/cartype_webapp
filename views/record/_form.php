<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Record */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="record-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'spz')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'carType')->dropDownList([ 'offroad' => 'Offroad', 'personal' => 'Personal', 'truck' => 'Truck', 'van' => 'Van', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'tstamp')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
