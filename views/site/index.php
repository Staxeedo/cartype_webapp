<?php

/* @var $this yii\web\View
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $categoryID */
$this->title = 'Registr Roušek';
?>
<div class="site-index">


    <div class="body-content">

        <div class="row">
            <div class="vyber">

                <div style="font-size: 20px;" class="col-md-9">

                <?php $form = ActiveForm::begin();?>

                <?=$form->field($model, 'cardNumber', ['inputOptions' =>

    ['autofocus' => 'autofocus', 'class' => 'form-control', 'style' => 'font-size:30px;text-align:center;height:100px;']])?>

                <div style="text-align: center;" class="form-group">
                    <?=Html::submitButton('OK', ['class' => 'btn btn-success', 'style' => 'font-size:20px;padding:25px 100px 25px 100px;'])?>
                </div>

                <?php ActiveForm::end();?>

                </div>
            </div>
        </div>


        <div class="col-md-1">

        </div>

    </div>

</div>

</div>
