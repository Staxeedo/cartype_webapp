<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Record */

$this->title = $model->recordID;
$this->params['breadcrumbs'][] = ['label' => 'Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="record-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->recordID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->recordID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'recordID',
            'spz',
            'carType',
            'tstamp',
        ],
    ]) ?>

    <div class="row">
    <div class="col-md-12">
    <div class="col-md-2"></div>
    <div class="col-md-8">
    <?= Html::img('../../uploads/'.$model->carType.'/'.$model->recordID.'a.jpg',['style'=>'width:500px']);?>
    </div>
    <div class="col-md-4"></div>
    </div>
    </div>

</div>
