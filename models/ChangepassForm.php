<?php

namespace app\models;

use yii\base\Model;

/**
 * Signup form
 */
class ChangepassForm extends Model
{
    public $password;
    public $repeat;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            ['password', 'required', 'message' => 'Tato položka musí být vyplněna'],
            ['repeat', 'required', 'message' => 'Heslo je nutné zopakovat.'],
            ['repeat', 'compare', 'compareAttribute' => 'password', 'message' => "Hesla se neshodují."],
        ];
    }
}
